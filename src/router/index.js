import { createRouter, createWebHashHistory } from "vue-router";
import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import Visualdictionary from "../views/Visualdictionary.vue";
import Register from "../views/Register.vue";
import Keyboard from "../views/Keyboard.vue";

const routes = [
  {
    path: "/Keyboard",
    name: "Keyboard",
    component: Keyboard,
  },
  {
    path: "/login",
    name: "login",
    component: Login,
  },
  {
    path: "/register",
    name: "register",
    component: Register,
  },
  {
    path: "/",
    name: "home",
    component: Home,
  },
  {
    path: "/Visualdictionary",
    name: "Visualdictionary",
    component: Visualdictionary,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
];

const router = createRouter({
  mode: "history",
  history: createWebHashHistory(),
  routes,
});

export default router;
// import Vue from 'vue'
// import Router from 'vue-router'
// import home from '@/components/home'

// Vue.use(Router)

// export default new Router({
//   routes: [
//     {
//       path: '/',
//       name: 'home',
//       component: home
//     }
//   ]
// })
