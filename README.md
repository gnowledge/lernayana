# lernayana_project

## Aim of lernaYana
The aim of this project is to create a website for teaching naYana script in an interactive way.

### About naYana Script
 - The naYana phonetic alphabet is created by Nagarjuna G. and Vickram Crishna and few other collaborators and interns at the gnowledge lab (https://www.gnowledge.org) of Homi Bhabha Centre for Science Education (https://www.hbcse.tifr.res.in), Tata Institute of Fundamental Research (TIFR) at Mumbai in India.
- Common code does not eliminate diversity of expression within a wider population, on the other hand it becomes a base for inclusive participation. Trascriptional unity can generate translational diversity is well evidenced by a common genetic code, where four letters and 64 words generated the organic diversity which is key for organic evolution. We hope naYana project will enhance cultural diversity and localization through transciptional unity and universal literacy.
- For further information on naYana, visit https://www.gnowledge.org/projects/naYana
### Features of lernaYana
Some features implemented in the website are as follows:
 - Designed beginner and intermediate quizzes in the form of MCQs to help people learn naYana.
 - Integrated naYana keyboard which converts the input into its corresponding IPA and naYana format.
 - Visual Dictionary with all naYana script characters with their SVG animation
 - User login and registration.


## Creating the animated SVGs

1. Using Pen Tool in Inkscape, defined the path which will be drawn while writing the character
2. Used this path and embedded it into the <svg> tag in HTML of Visual Dictionary page
3. Using JavaScript triggered the animation when user hovers over this character and set the colour and duration of animation
4. Using JavaScript, got the path length of every character and created a transition in ‘stroke-dasharray’ and ‘stroke-dashoffset’ properties to transition from 0 to path’s length


## Linking the "Start Learning" button on the home page to the wordpress
We can't directly link the  "Start Learning" button on the home page of the lernayana project to our Wordpress site because currently the lernayana project and the wordpress isn't hosted. So after hosting the lernayana Vue project and the wordpress, we have to do the following steps in Wordpress - 
1. Go to the Home.vue file in 'src/views/Home.vue'
2. Put the link of the hosted wordpress in the href property of the anchor(a) tag where "Start Learning" is written (Line 11).
3. Save and run the project.

## How to use other form of questionnaire in H5P
After hosting the Wordpress site after following the Readme file of Wordpress folder, we can modify the H5P content to our liking. The instruction that follows will guide you how to modify H5P in Wordpress file.
1. Enter the dashboard of the Wordpress site.
2. Under plugins, go to H5P.
3. In H5P, go to add new and you are set to make your own game.
4. Then with elementor use H5P id to embed the H5P you created in the Wordpress Site. 

For information on how to get different games through H5P, visit https://h5p.org/content-types-and-applications .

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
