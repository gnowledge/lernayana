[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
# Wordpress

## Introduction

> WordPress is a website builder and content management system. It is an open source software that anyone can use to make any kind of website imaginable. It started out as a blogging platform in 2003 but soon transformed into a CMS and later a full-fledged website building platform.(https://www.wpbeginner.com/)

## H5P

> H5P is short for HTML5 Package and is a simple way to create and share rich and interactive web content. ... H5P can be integrated with learning platforms and content management systems and allows teachers to create rich interactive content and students to experience them on laptops, tablets or smartphones.(https://h5p.org/)

## Installation( How to use the .wpress file)

> 1. Install Wordpress on the Webhost and login to your wordpress account.
> 2. After installing WordPress, click get started. Then, you will see Wordpress will be installed on the webhost with its default content.
> 3. Now install the plugin All-in-One WP Migration(https://wordpress.org/plugins/all-in-one-wp-migration/) on both the local site and on the live website.
> 4. After installing the plugin, go to local host dashboard and use All-in-One WP Migration plugin to export in form of a file.
> 5. The file provided in this folder is the file that we have got from step 4.
> 6. Now inside the dashboard of the live website, go to All-in-One WP Migration and import the Wordpress file provided in this folder(the file mentioned in step 5).
> 7. Now our Wordpress site is transferred from local host to our hosting service. Now in order to change the contents of our site, type /login after the domain of our site.
> 8. You have to use the username and password of the local host Wordpress files to customize the hosted Wordpress site. 

## Instruction to link the Home Menu of the Wordpress site to Lernayana Home page 
> We can't directly link the home page of the lernayana project to our Wordpress site because currently the lernayana project isn't hosted. So after hosting the lernayana Vue project, we have to do the following steps in Wordpress.

> 1. Open the hosted Wordpress site and type /login after the domain name of our Wordpress site.
> 2. Enter the credentials of the local Wordpress site after /login and enter the dashboard.
> 3. Go to Appearnace->Menus
> 4. Under the Menu Structure, click on the custom link dropdown.
> 5. Now change the url to the url of the home page of oue lernayana project.

